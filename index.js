// 3-4

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(object => {
    const titles = object.map((item, index) => `${index+1}. ${item.title}`);
    console.groupCollapsed('Array (200)');
    titles.forEach(title => console.log(title));
    console.groupEnd();
});

// 5-6

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => res.json())
.then(object => console.log(`The item "${object.title}" on the list has a status of ${object.completed}`))


// 7

fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        completed: false,
        title: 'Create To Do List Item',

        userID: 1
    })
})

.then((response) => response.json())
.then(object => console.log(object));

// 8-9

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        'dateCompleted': 'Pending',
        'description': 'To update the my to do list with a different data structure.',
        'status': 'Pending',
        'title': 'Updated To Do List Item',
        'userID': 1
    })
})

.then((response) => response.json())
.then(object => console.log(object));

// 10-11

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        'status': 'Complete',
        'dateCompleted': '07/09/21'
    })
})

.then((response) => response.json())
.then(object => console.log(object));

// 12

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE'
})

.then(response => response.json())
.then(object => console.log(object));